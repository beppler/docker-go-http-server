FROM scratch

MAINTAINER Carlos Alberto Costa Beppler <beppler@gmail.com>

EXPOSE 8080

ADD go-http-server /

ENTRYPOINT ["/go-http-server"]
