package main

import (
	"io"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	io.WriteString(w, "Method:\n    ")
	io.WriteString(w, r.Method)
	io.WriteString(w, "\n")
	io.WriteString(w, "URL:\n    ")
	io.WriteString(w, r.URL.String())
	io.WriteString(w, "\n")
	io.WriteString(w, "Host:\n    ")
	io.WriteString(w, r.Host)
	io.WriteString(w, "\n")
	for header, values := range r.Header {
		io.WriteString(w, header)
		io.WriteString(w, ":\n")
		for _, value := range values {
			io.WriteString(w, "    ")
			io.WriteString(w, value)
			io.WriteString(w, "\n")
		}
	}
}

func main() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":8080", nil)
}
