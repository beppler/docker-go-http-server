all: build

build: go-http-server
	docker build --rm -t go-http-server .

go-http-server: go-http-server.go
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-s' -o go-http-server -x .

clean:
	rm -f go-http-server
	docker images | grep go-http-server | awk '{ print $$3 }' | xargs -r docker rmi

run: 
	docker run -d -p 8080:8080 --name go-http-server go-http-server

